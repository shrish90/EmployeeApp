package DAO.DAOImpl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import DAO.PersistEmployeeDAO;
import model.Address;
import model.Employee;
@Component
public class PersistEmployeeDAOImpl implements PersistEmployeeDAO{
	@Autowired
	JdbcTemplate jdbcTemplate;
	public Employee persisteEmployee(Employee employee) {
		jdbcTemplate.update("INSERT INTO employees(employee_number,employee_name,employee_email,department_id) values (?,?,?,?)",
				employee.getEmpID(),
				employee.getName(),
				employee.getEmailId(),
				employee.getDepartment()
				);
		SimpleJdbcInsert jdbcObj = new SimpleJdbcInsert(jdbcTemplate).withTableName("address");
		for(Address address:employee.getAddress()){
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("emp_id", employee.getEmpID());
			map.put("street", address.getStreet());
			map.put("pincode", address.getPinCode());
			jdbcObj.execute(map);
		}
		return employee;
	}

}
