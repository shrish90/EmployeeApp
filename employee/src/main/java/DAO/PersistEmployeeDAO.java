package DAO;

import model.Employee;

public interface PersistEmployeeDAO {
	Employee persisteEmployee(Employee employee);
}
