package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import model.Employee;
import service.SetEmployeeService;

@Controller
@RequestMapping("/employee")
public class SaveEmployeeController {
@Autowired
SetEmployeeService setEmployee;
@RequestMapping(value="details", method = RequestMethod.POST)
public @ResponseBody Employee saveEmployeeDetails(@RequestBody Employee employee){
	Employee emp  = setEmployee.saveEmployee(employee);
	return emp;
}
}
