package service;

import model.Employee;

public interface SetEmployeeService {
	Employee saveEmployee(Employee emp);
}
