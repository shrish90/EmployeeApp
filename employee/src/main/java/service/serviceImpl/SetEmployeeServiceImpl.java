package service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DAO.PersistEmployeeDAO;
import model.Employee;
import service.SetEmployeeService;

@Service
public class SetEmployeeServiceImpl implements SetEmployeeService{
	@Autowired
	PersistEmployeeDAO peDao;
	public Employee saveEmployee(Employee emp) {
			peDao.persisteEmployee(emp);
		return emp;
	}

}
