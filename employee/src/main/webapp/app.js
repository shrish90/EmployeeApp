var app = angular.module("employeeApp",[]);
app.controller('employeeCtrl', function($scope,$http) {
	$scope.showMsg = false;
	$scope.submitDetails = function(){
		$scope.employeeObj = {
				"empID":$scope.emp_id,
				"name":$scope.emp_name,
				"emailId":$scope.emp_email,
				"department":$scope.emp_department,
				"address":[
					        {
								"street":$scope.emp_add1,
								"pinCode":$scope.emp_add1pin
							},
							
							{
								"street":$scope.emp_add2,
								"pinCode":$scope.emp_add2pin
							}
				        ]
			};
		$http.post("http://localhost:8080/employee/service/employee/details",$scope.employeeObj).then(
			       function(response){
			    	   $scope.showMsg = true;
			         });
	};
    
});